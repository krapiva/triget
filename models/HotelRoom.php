<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "hotelRooms".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $photo
 *
 * @property Booked[] $bookeds
 */
class HotelRoom extends \yii\db\ActiveRecord
{
    /**
     * @var string директория сохранения фото
     */
    public static $uploadPath = 'uploads/img/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hotel_rooms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            ['title', 'unique'],
            [['title', 'description'], 'string', 'max' => 255],
            ['photo', 'file', 'extensions' => 'png, jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'description' => 'Краткое описание',
            'photo' => 'Фотография',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooked()
    {
        return $this->hasMany(Booked::className(), ['hotelRoomId' => 'id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->uploadFile('photo', false);
            return !$this->hasErrors('photo');
        } else {
            return false;
        }
    }


    protected function uploadFile($attribute, $required = false)
    {
        $file = UploadedFile::getInstance($this, $attribute);

        if ($file === null && $required && $this->isNewRecord) {
            $this->addError($attribute, "Пожалуйста выберите «{$this->getAttributeLabel($attribute)}»");
            return false;
        }

        if ($file !== null) {
            $filename = $this->generateFilename($file->name, $file->extension);
            $this->$attribute = $filename;
            $file->saveAs($filename);
        } else {
            $this->$attribute = isset($this->oldAttributes[$attribute]) ? $this->oldAttributes[$attribute] : '';
        }

        return true;
    }

    /**
     * Возвращает имя фото для имени $filename и расширением $ext.
     *
     * @param string $filename имя фото
     * @param string $ext расширение
     * @return string
     */
    public static function generateFilename($filename, $ext)
    {
        FileHelper::createDirectory(self::$uploadPath);

        return self::$uploadPath . md5(time() . $filename) . '.' . $ext;
    }

}
