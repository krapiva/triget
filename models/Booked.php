<?php

namespace app\models;

use app\behaviors\MyTimestampBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "booked".
 *
 * @property int $id
 * @property int $hotelRoomId
 * @property string $username
 * @property string $phone
 * @property int $dayStart
 * @property int $dayFinish
 * @property int $createdAt
 * @property int $updatedAt
 *
 * @property HotelRoom $hotelRoom
 */
class Booked extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'booked';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => TimestampBehavior::className(),
            'createdAtAttribute' => 'createdAt',
            'updatedAtAttribute' => 'updatedAt',
        ];
        $behaviors[] =  [
            'class' => MyTimestampBehavior::className(),
            'attribute' => 'dayStart, dayFinish',
            'format' => 'dd.MM.yyyy',
        ];

        return $behaviors;
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hotelRoomId', 'dayStart', 'dayFinish', 'username', 'phone'], 'required'],
            [['hotelRoomId', 'createdAt', 'updatedAt'], 'integer'],
            [['dayStart', 'dayFinish'], 'date', 'format' => 'dd.MM.yyyy'],
            [['username', 'phone'], 'string', 'max' => 255],
            ['phone', 'validPhone'],
            [['dayStart', 'dayFinish'], 'validDate'],
            [['hotelRoomId'], 'exist', 'skipOnError' => true, 'targetClass' => HotelRoom::className(), 'targetAttribute' => ['hotelRoomId' => 'id']],
        ];
    }

    /**
     * @param $attribute
     * проверяем телефон
     */
    public function validPhone($attribute)
    {
        $phone = preg_replace('/[^0-9]/', '', $this->$attribute);
        $this->$attribute = $phone;
        if (!preg_match('/^[7]/', $phone)) {
            $this->addError($attribute, 'Номер телефона должен начинться с 7');
        }
    }

    /**
     * @param $attribute
     * Проверяем дату на правильность
     */
    public function validDate($attribute) {
        $start = strtotime($this->dayStart);
        $finish = strtotime($this->dayFinish);
        if ($start > $finish) {
            $this->addError($attribute, 'Дата заезда не может быть позже даты выезда');
        }
        $booked = self::find()->where(['hotelRoomId' => $this->hotelRoomId])->andWhere(['or',
            ['and',
                [ '<=', 'dayStart', $start],
                [ '>=', 'dayFinish', $start]
            ],
            ['and',
                [ '>=', 'dayStart', $start],
                [ '<=', 'dayStart', $finish]
            ],
        ])->one();
        if ($booked) {
            $this->addError($attribute, 'На данный период номер уже забронирован');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hotelRoomId' => 'Номер',
            'username' => 'Клиент',
            'phone' => 'Телефон',
            'dayStart' => 'Дата заезда',
            'dayFinish' => 'Дата выезда',
            'createdAt' => 'Дата создания',
            'updatedAt' => 'Дата обновления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelRoom()
    {
        return $this->hasOne(HotelRoom::className(), ['id' => 'hotelRoomId']);
    }
}
