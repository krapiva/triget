<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180504_051445_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'createdAt' => $this->integer()->notNull(),
            'updatedAt' => $this->integer()->notNull(),
        ]);

        $this->createTable('hotel_rooms', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description' => $this->string(),
            'photo' => $this->string()
        ]);

        $this->createTable('booked', [
            'id' => $this->primaryKey(),
            'hotelRoomId' => $this->integer(),
            'username' => $this->string(),
            'phone' => $this->string(),
            'dayStart' => $this->integer()->notNull(),
            'dayFinish' => $this->integer()->notNull(),
            'createdAt' => $this->integer()->notNull(),
            'updatedAt' => $this->integer()->notNull()
        ]);

        $this->addForeignKey('fk-booked-hotelRoomId', 'booked', 'hotelRoomId', 'hotel_rooms', 'id', 'CASCADE');

        $user = new \app\models\User();
        $user->username = 'admin';
        $user->email = 'admin@admin.com';
        $user->password_hash = Yii::$app->security->generatePasswordHash('admin');
        $user->password_reset_token = Yii::$app->security->generateRandomString();
        $user->auth_key = Yii::$app->security->generateRandomString();
        $user->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-booked-hotelRoomId', 'booked');
        $this->dropTable('users');
        $this->dropTable('hotel_rooms');
        $this->dropTable('booked');
    }
}
