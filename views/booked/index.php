<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список броней';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booked-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'hotelRoomId',
                'value' => function($data) {
                    return $data->hotelRoomId ? $data->hotelRoom->title : null;
                }
            ],
            'username',
            'phone',
            [
                'attribute' => 'date',
                'label' => 'Дата бронирования',
                'value' => function ($data) {
                    return $data->dayStart . ' - ' . $data->dayFinish;
                }
            ],
            'createdAt:datetime'
        ],
    ]); ?>
</div>
