<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Booked */

$this->title = 'Забронировать номер: ' . $model->hotelRoom->title;
$this->params['breadcrumbs'][] = ['label' => 'Список номеров', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booked-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
