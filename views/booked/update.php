<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Booked */

$this->title = 'Update Booked: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bookeds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="booked-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
