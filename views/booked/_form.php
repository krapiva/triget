<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Booked */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="booked-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'hotelRoomId')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dayStart')->widget(DatePicker::className(), [
        'name' => 'dayStart',
        'language' => 'ru',
        'value' => $model->dayStart,
        'pluginOptions' => [
            'format' => 'dd.mm.yyyy',
            'todayHighlight' => true
        ]
    ]) ?>

    <?= $form->field($model, 'dayFinish')->widget(DatePicker::className(), [
        'name' => 'dayFinish',
        'language' => 'ru',
        'value' => $model->dayFinish,
        'pluginOptions' => [
            'format' => 'dd.mm.yyyy',
            'todayHighlight' => true,
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
