<?php

use yii\helpers\Html;
use yii\grid\GridView;
use himiklab\thumbnail\EasyThumbnailImage;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список номеров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booked-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('save')): ?>

        <div class="col-md-12 btn-block alert block-success btn-success"><?=Yii::$app->session->getFlash('save')?></div>
        <script>
            setTimeout(function () {
                document.querySelector('.alert.block-success').remove();
            }, 4000);
        </script>

    <?php endif; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'booked',
                'label' => '',
                'format' =>'raw',
                'value' => function($data) {
                    return Html::a('Забронировать', ['create', 'roomId' => $data->id], ['class' => 'btn btn-success']);
                }
            ],
            'title',
            'description',
            [
                'attribute' => 'photo',
                'format' =>'html',
                'value' => function($data) {
                    return EasyThumbnailImage::thumbnailImg(
                        $data->photo,
                        400,
                        300,
                        EasyThumbnailImage::THUMBNAIL_INSET
                    );
                },
                'filter' => false
            ],
        ],
    ]); ?>
</div>
