<?php

use yii\helpers\Html;
use yii\grid\GridView;
use himiklab\thumbnail\EasyThumbnailImage;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HotelRoomSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список номеров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-room-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить номер', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'description',
            [
                'attribute' => 'photo',
                'format' =>'html',
                'value' => function($data) {
                    if ($data->photo) {
                        return EasyThumbnailImage::thumbnailImg(
                            $data->photo,
                            400,
                            300,
                            EasyThumbnailImage::THUMBNAIL_INSET
                        );
                    }
                    return '';
                },
                'filter' => false
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
