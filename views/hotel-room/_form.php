<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use himiklab\thumbnail\EasyThumbnailImage;

/* @var $this yii\web\View */
/* @var $model app\models\HotelRoom */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hotel-room-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
    <?php if ($model->photo) : ?>
        <?= EasyThumbnailImage::thumbnailImg(
            $model->photo,
            400,
            300,
            EasyThumbnailImage::THUMBNAIL_INSET
        ) ?>
        <div class="clearfix"></div>
    <?php endif; ?>

    <?= $form->field($model, 'photo')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
