<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\HotelRoom */

$this->title = 'Добавить номер';
$this->params['breadcrumbs'][] = ['label' => 'Список номеров', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-room-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
