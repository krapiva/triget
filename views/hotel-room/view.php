<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\HotelRoom */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Список номеров', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-room-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'description',
        ],
    ]) ?>

</div>
<h3>Список броней</h3>

<div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'phone',
            [
                'attribute' => 'date',
                'label' => 'Дата бронирования',
                'value' => function ($data) {
                    return $data->dayStart . ' - ' . $data->dayFinish;
                }
            ],
            'createdAt:datetime'
        ],
    ]); ?>
</div>

